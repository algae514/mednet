Adam global bizlist 
===============================


###1. Technologies used
* Spring 4.2.2.RELEASE
* Jackson 2.6.3
* jQuery 1.10.2
* Boostrap 3
* Maven 3
* mysql

###2. To Run this project locally
```shell
execute sql dbSetup.sql in bizlist\src\main\resources location
$ git clone https://uma99@bitbucket.org/uma99/bizlist.git
$ mvn clean install
$ mvn jetty:run

Existing REST calls available as postman project export bizlist.postman_collection.json in bizlist\src\main\resources location

digital ocean server setup : 

Tomcat
https://www.digitalocean.com/community/tutorials/how-to-install-apache-tomcat-7-on-ubuntu-14-04-via-apt-get

My SQL
https://www.cyberciti.biz/faq/howto-install-mysql-on-ubuntu-linux-16-04/




```
Access ```http://localhost:8080/web```

###3. To import this project into Eclipse IDE
1. ```$ mvn eclipse:eclipse```
2. Import into Eclipse via **existing projects into workspace** option.
3. Done.

###4. Project Demo

###4. Refrence links
http://www.mkyong.com/spring-mvc/spring-4-mvc-ajax-hello-world-example/
http://websystique.com/springmvc/spring-4-mvc-and-hibernate4-integration-example-using-annotations/
