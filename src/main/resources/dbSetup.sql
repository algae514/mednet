-- --------------------------------------------------------
-- Host:                         127.0.0.1
-- Server version:               5.7.16-log - MySQL Community Server (GPL)
-- Server OS:                    Win64
-- HeidiSQL Version:             9.4.0.5125
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;


-- Dumping database structure for mednet
DROP DATABASE IF EXISTS `mednet`;
CREATE DATABASE IF NOT EXISTS `mednet` /*!40100 DEFAULT CHARACTER SET utf8 */;
USE `mednet`;

-- Dumping structure for table mednet.aptmnt
DROP TABLE IF EXISTS `aptmnt`;
CREATE TABLE IF NOT EXISTS `aptmnt` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `dctr_id` int(11) DEFAULT NULL,
  `ptnt_id` int(11) DEFAULT NULL,
  `from_time` datetime DEFAULT NULL,
  `to_time` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `id` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=18 DEFAULT CHARSET=utf8;

-- Dumping data for table mednet.aptmnt: ~17 rows (approximately)
/*!40000 ALTER TABLE `aptmnt` DISABLE KEYS */;
REPLACE INTO `aptmnt` (`id`, `dctr_id`, `ptnt_id`, `from_time`, `to_time`) VALUES
	(1, 1, 3, '2016-12-12 14:23:45', '2016-12-12 14:23:45'),
	(2, 1, 3, '2016-11-17 00:00:00', '2016-11-17 00:00:00'),
	(3, 1, 3, '2016-11-17 00:00:00', '2016-11-17 00:00:00'),
	(4, 1, 3, '2016-11-10 01:00:00', '2016-11-10 01:00:00'),
	(5, 1, 1, '2016-11-24 12:59:00', '2016-11-24 12:59:00'),
	(6, 1, 3, '2016-12-12 14:23:45', '2016-12-12 14:23:45'),
	(7, 1, 3, '2016-12-12 14:23:45', '2016-12-12 14:23:45'),
	(8, 1, 3, '2016-12-12 14:23:45', '2016-12-12 14:23:45'),
	(9, 1, 3, '2016-12-12 14:23:45', '2016-12-12 14:23:45'),
	(10, 1, 3, '2016-12-12 14:23:45', '2016-12-12 14:23:45'),
	(11, 1, 3, '2016-12-12 14:23:45', '2016-12-12 14:23:45'),
	(12, 1, 3, '2016-12-12 14:23:45', '2016-12-12 14:23:45'),
	(13, 1, 3, '2016-12-12 14:23:45', '2016-12-12 14:23:45'),
	(14, 1, 3, '2016-12-12 14:23:45', '2016-12-12 14:23:45'),
	(15, 1, 3, '2016-12-12 14:23:45', '2016-12-12 14:23:45'),
	(16, 2, 3, '2016-12-12 14:23:45', '2016-12-12 14:23:45'),
	(17, 2, 3, '2016-12-12 14:23:45', '2016-12-12 14:23:45');
/*!40000 ALTER TABLE `aptmnt` ENABLE KEYS */;

-- Dumping structure for table mednet.dctr
DROP TABLE IF EXISTS `dctr`;
CREATE TABLE IF NOT EXISTS `dctr` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) DEFAULT NULL,
  `lcn` varchar(50) DEFAULT NULL,
  `userid` varchar(50) NOT NULL,
  `password` varchar(50) DEFAULT NULL,
  `image` varchar(500) DEFAULT NULL,
  `qualfcn` varchar(50) DEFAULT NULL,
  `issue` varchar(50) DEFAULT NULL,
  `price` varchar(50) DEFAULT NULL,
  `ct` varchar(50) DEFAULT NULL,
  `trtmnt` varchar(50) DEFAULT NULL,
  `avlblTime` varchar(50) DEFAULT NULL,
  `rcmnds` int(11) DEFAULT '0',
  PRIMARY KEY (`ID`),
  UNIQUE KEY `userid_unique` (`userid`),
  KEY `ID` (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=60 DEFAULT CHARSET=utf8;

-- Dumping data for table mednet.dctr: ~6 rows (approximately)
/*!40000 ALTER TABLE `dctr` DISABLE KEYS */;
REPLACE INTO `dctr` (`ID`, `name`, `lcn`, `userid`, `password`, `image`, `qualfcn`, `issue`, `price`, `ct`, `trtmnt`, `avlblTime`, `rcmnds`) VALUES
	(1, 'Dr. abc', 'tadi', 'balu.in.u@gmail.com', 'kinngsay@123', 'http://static4.depositphotos.com/1006542/369/i/950/depositphotos_3694951-stock-photo-sexy-doctor.jpg', NULL, 'Fever', '150.25', 'Vijaynagaram', 'Fever', NULL, 3),
	(4, 'Balaji', 'Madhapur', 'killbill@gmail.com', 'pas', 'http://worldcareintl.wpengine.netdna-cdn.com/wp-content/uploads/2015/11/DoctorMan.png', 'MBBS', NULL, '400.0', 'Hyderabad', 'Fever', NULL, 0),
	(5, 'Mohan V', 'Dilshuk Nagar', 'mohanvuppuluri@gmail.com', 'pas', 'https://www.simpleonlinepharmacy.co.uk/wp-content/uploads/2015/03/Online-Doctor-UK.jpg', 'BDS', NULL, '300.0', 'Hyderabad', NULL, NULL, 0),
	(6, 'Admin Ji', 'Benz circle', 'admin@gamifad.com', 'pas', 'http://www.doctorsbillingflorida.com/wp-content/uploads/2015/06/male-doctor.png', 'MDS', NULL, '400.0', 'Vijaywada', NULL, NULL, 0),
	(7, 'Abc Xyz', 'Bezwada', 'abc@gmail.com', 'pas', 'http://healthathomes.com/wp-content/uploads/bb_florida-ent-doctor.png', 'MBBS , MD', NULL, '500.0', 'Vijaywada', NULL, NULL, 0),
	(55, 'Abc Xyz', 'Bezwada', 'abc1@gmail.com', 'pas', 'http://healthathomes.com/wp-content/uploads/bb_florida-ent-doctor.png', 'MBBS , MD', NULL, '500.0', 'Vijaywada', NULL, NULL, 0);
/*!40000 ALTER TABLE `dctr` ENABLE KEYS */;

-- Dumping structure for table mednet.lcn
DROP TABLE IF EXISTS `lcn`;
CREATE TABLE IF NOT EXISTS `lcn` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `ct` varchar(100) DEFAULT NULL,
  `lcn` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `id` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=20 DEFAULT CHARSET=utf8;

-- Dumping data for table mednet.lcn: ~7 rows (approximately)
/*!40000 ALTER TABLE `lcn` DISABLE KEYS */;
REPLACE INTO `lcn` (`id`, `ct`, `lcn`) VALUES
	(13, 'Vijaynagaram', 'chitrakota'),
	(14, 'Shrikakulam', 'Rajam'),
	(15, 'Shrikakulam', 'garvidi'),
	(16, 'Shrikakulam', 'dhulpeta'),
	(17, 'Vijaynagaram', 'rangada'),
	(18, 'Vijaynagaram', 'duvva'),
	(19, 'Vijaynagaram', 'bobbili');
/*!40000 ALTER TABLE `lcn` ENABLE KEYS */;

-- Dumping structure for table mednet.ptnt
DROP TABLE IF EXISTS `ptnt`;
CREATE TABLE IF NOT EXISTS `ptnt` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) DEFAULT NULL,
  `userid` varchar(50) DEFAULT NULL,
  `password` varchar(50) DEFAULT NULL,
  `cd` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `id` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8;

-- Dumping data for table mednet.ptnt: ~3 rows (approximately)
/*!40000 ALTER TABLE `ptnt` DISABLE KEYS */;
REPLACE INTO `ptnt` (`id`, `name`, `userid`, `password`, `cd`) VALUES
	(1, 'Kingadal', 'kin', 'kin', NULL),
	(2, 'Kingadal', 'kin2', 'kinngsay@123', NULL),
	(3, 'Kingadal', 'admin@gamifad.com', 'kinngsay@123', NULL);
/*!40000 ALTER TABLE `ptnt` ENABLE KEYS */;

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
