	jQuery(document).ready(function($) {

		$("#search-form").submit(function(event) {
			// Disble the search button
			enableSearchButton(false);
			// Prevent the form from submitting via the browser.
			event.preventDefault();
			searchViaAjax();
		});

	});

	function searchViaAjax() {
		console.log("  searchViaAjax()  ")

		var search = {}
		search["userid"] = $("#username").val();
		search["password"] = $("#email").val();

		$.ajax({
			type : "POST",
			contentType : "application/json",
			url : home+"doctor/api/login",
			data : JSON.stringify(search),
			dataType : 'json',
			timeout : 100000,
			success : function(data) {
				console.log("SUCCESS: ", data);
				display(data);
				if(data.code == "200"){
					window.location.assign(home+"home")
				}
			},
			error : function(e) {
				console.log("ERROR: ", e);
				display(e);
			},
			done : function(e) {
				console.log("DONE");
				enableSearchButton(true);
			}
		});

	}

	function enableSearchButton(flag) {
		$("#btn-search").prop("disabled", flag);
	}

	function display(data) {
		var json = "<h4>Ajax Response</h4><pre>"
				+ JSON.stringify(data, null, 4) + "</pre>";
		$('#feedback').html(json);
	}
