package com.adamglobal.web.controller;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.adamglobal.services.AppointmentService;
import com.adamglobal.services.EmailService;
import com.adamglobal.services.PatientService;
import com.adamglobal.web.jsonview.Views;
import com.adamglobal.web.model.Appointment;
import com.adamglobal.web.model.Patient;
import com.adamglobal.web.model.Patient;
import com.adamglobal.web.model.PatientResponseBody;
import com.adamglobal.web.model.PatientSearchCriterea;
import com.fasterxml.jackson.annotation.JsonView;

@CrossOrigin(maxAge = 3600)
@RestController
public class PatientController {

	List<Patient> patients;
	
	Logger logger = Logger.getLogger(PatientController.class.getName());
	
	@Autowired
	PatientService service;
	
	@Autowired
	AppointmentService appointmentService;
	
	@Autowired
	EmailService emailService;
	
	     
	
	
	/**
	 * 
	 * @param Patient
	 * @return SUCCESS / FAILURE / USER_ALREADY_EXSISTS
	 */
	@JsonView(Views.Public.class)
	@RequestMapping(value = "/doctor/api/registerpatient")
	public PatientResponseBody registerNewPatient(@RequestBody Patient newPatient) {
		logger.log(Level.INFO, "Registered Patient: "+newPatient);
		PatientResponseBody result = new PatientResponseBody();
		result.setCode("200");
		Patient findPatientByUserid = service.findPatientByUserid(newPatient.getUserid());
		if(findPatientByUserid == null){
			service.savePatient(newPatient);	
		}
		result.setMsg("username already exists, Please choose different username.");
		return result;

	}

	/**
	 * 
	 * @param Patient
	 * @return SUCCESS / FAILURE
	 */
	
	@JsonView(Views.Public.class)
	@CrossOrigin(maxAge = 3600)
	@RequestMapping(value = "/doctor/api/loginpatient")
	public PatientResponseBody loginPatient(@RequestBody Patient patient ) {

		logger.log(Level.INFO, "Login Patient: input provided : "+patient);
		
		PatientResponseBody result = new PatientResponseBody();
		Patient patientView = service.searchPatientsAuth(patient);
		
		logger.log(Level.INFO, "patientView : "+patientView);
		
		patients = new ArrayList<Patient>();
		if(patientView!=null){
			patients.add(patientView);
			logger.log(Level.INFO, "patients  : "+patients);
			result.setResult(patients);
			
			result.setCode("200");
			result.setMsg("SUCCESS");
			logger.log(Level.INFO, " result.getResult : "+result);
		}
		else{
			result.setMsg("FAIL");
		}
		
		return result;
	}

	
	/**
	 * 
	 * @param Patient
	 * @return EMAIL_SENT / EMAIL_NOT_FOUND
	 */
	@JsonView(Views.Public.class)
	@RequestMapping(value = "/doctor/api/fyppatient")
	public PatientResponseBody fypPatient(@RequestBody Patient patient) {
		logger.log(Level.INFO, "FYP Patient: "+patient);
		PatientResponseBody result = new PatientResponseBody();
		result.setCode("200");
//		String patientCreatedMessage = patientService.sendEmail(patient);
		String patientCreatedMessage = "SUCCESS";
		result.setMsg(patientCreatedMessage);
		return result;
	}

	
	/**
	 * 
	 * @param Patient
	 * @return EMAIL_SENT / EMAIL_NOT_FOUND
	 */
	@JsonView(Views.Public.class)
	@RequestMapping(value = "/doctor/api/viewProfilepatient")
	public PatientResponseBody viewProfile(@RequestBody Patient patient) {
		logger.log(Level.INFO, "ViewProfile Patient: "+patient);
		
		PatientResponseBody result = new PatientResponseBody();
		result.setCode("200");
//		Patient patientView = patientService.viewProfile(patient);
		Patient patientView = service.findPatientByUserid(patient.getUserid());
		patients = new ArrayList<Patient>();
		patients.add(patientView);
		result.setResult(patients);

		return result;
	}
	
	
	/**
	 * 
	 * @param Patient
	 * @return EMAIL_SENT / EMAIL_NOT_FOUND
	 */
	@JsonView(Views.Public.class)
	@RequestMapping(value = "/doctor/api/bookappointment")
	public PatientResponseBody bookAppointment(@RequestBody Appointment appointment) {
		logger.log(Level.INFO, "ViewProfile Patient: "+appointment);
		
		PatientResponseBody result = new PatientResponseBody();
		result.setCode("200");
		appointmentService.saveAppointment(appointment);
		emailService.sendEmail(appointment);
		patients = new ArrayList<Patient>();
		result.setResult(patients);
		System.out.println(" -------------- FIND BY ID -----------------------");
		
		return result;
	}
	
	
	/**
	 * 
	 * @param Patient
	 * @return SUCCESS / FAILED
	 */
	@JsonView(Views.Public.class)
	@RequestMapping(value = "/doctor/api/editProfilepatient")
	public PatientResponseBody editProfile(@RequestBody Patient patient) {
		PatientResponseBody result = new PatientResponseBody();
		result.setCode("200");
		String patientCreatedMessage = service.updatePatient(patient);
		result.setMsg(patientCreatedMessage);
		return result;
	}
	
	
	
	// @ResponseBody, not necessary, since class is annotated with @RestController
	// @RequestBody - Convert the json data into object (SearchCriteria) mapped by field name.
	// @JsonView(Views.Public.class) - Optional, limited the json data display to client.
	@JsonView(Views.Public.class)
	@RequestMapping(value = "/doctor/api/searchpatient")
	public PatientResponseBody searchPatients(@RequestBody PatientSearchCriterea criterea) {
		logger.log(Level.INFO, "search Patient: "+criterea);
		PatientResponseBody result = new PatientResponseBody();
		
		if(criterea.isEmpty()){
			result.setCode("200");
			result.setCode("Null criterea. No objects present in criterea. recieved : "+criterea.toString());
			return result;
		}
		
		
		List<Patient> findAllPatients = service.searchPatients(criterea);
		result.setResult(findAllPatients);
		
		//AjaxResponseBody will be converted into json format and send back to client.
		return result;
		
	}
	

	// Init some patients for testing
	@PostConstruct
	private void iniDataForTesting() {
		patients = new ArrayList<Patient>();

	}

}
