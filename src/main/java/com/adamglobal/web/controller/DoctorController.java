package com.adamglobal.web.controller;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.adamglobal.services.DoctorService;
import com.adamglobal.web.jsonview.Views;
import com.adamglobal.web.model.Doctor;
import com.adamglobal.web.model.DoctorResponseBody;
import com.adamglobal.web.model.DoctorSearchCriterea;
import com.fasterxml.jackson.annotation.JsonView;

@CrossOrigin(maxAge = 3600)
@RestController
public class DoctorController {

	List<Doctor> doctors;
	
	Logger logger = Logger.getLogger(DoctorController.class.getName());
	
	@Autowired
	 DoctorService service;
	 
	     
	
	
	/**
	 * 
	 * @param Doctor
	 * @return SUCCESS / FAILURE / USER_ALREADY_EXSISTS
	 */
	@JsonView(Views.Public.class)
	@RequestMapping(value = "/doctor/api/register")
	public DoctorResponseBody registerNewDoctor(@RequestBody Doctor newDoctor) {
		logger.log(Level.INFO, "Registered Doctor: "+newDoctor);
		DoctorResponseBody result = new DoctorResponseBody();
		result.setCode("200");
		service.saveDoctor(newDoctor);
		return result;

	}

	/**
	 * 
	 * @param Doctor
	 * @return SUCCESS / FAILURE
	 */
	
	@JsonView(Views.Public.class)
	@CrossOrigin(maxAge = 3600)
	@RequestMapping(value = "/doctor/api/login")
	public DoctorResponseBody loginDoctor(@RequestBody Doctor doctor ) {

		logger.log(Level.INFO, "Login Doctor: input provided : "+doctor);
		
		DoctorResponseBody result = new DoctorResponseBody();
		Doctor doctorView = service.searchDoctorsAuth(doctor);
		
		logger.log(Level.INFO, "doctorView "+doctorView);
		
		doctors = new ArrayList<Doctor>();
		if(doctorView!=null){
			doctors.add(doctorView);
			result.setResult(doctors);
			result.setCode("200");
			result.setMsg("SUCCESS");
			
		}
		else{
			result.setMsg("FAIL");
		}
		
		return result;
	}

	
	/**
	 * 
	 * @param Doctor
	 * @return EMAIL_SENT / EMAIL_NOT_FOUND
	 */
	@JsonView(Views.Public.class)
	@RequestMapping(value = "/doctor/api/fyp")
	public DoctorResponseBody fypDoctor(@RequestBody Doctor doctor) {
		logger.log(Level.INFO, "FYP Doctor: "+doctor);
		DoctorResponseBody result = new DoctorResponseBody();
		result.setCode("200");
//		String doctorCreatedMessage = doctorService.sendEmail(doctor);
		String doctorCreatedMessage = "SUCCESS";
		result.setMsg(doctorCreatedMessage);
		return result;
	}
	
	/**
	 * 
	 * @param Doctor
	 * @return EMAIL_SENT / EMAIL_NOT_FOUND
	 */
	@JsonView(Views.Public.class)
	@RequestMapping(value = "/doctor/api/viewProfile")
	public DoctorResponseBody viewProfile(@RequestBody Doctor doctor) {
		logger.log(Level.INFO, "ViewProfile Doctor: "+doctor);
		
		DoctorResponseBody result = new DoctorResponseBody();
		result.setCode("200");
//		Doctor doctorView = doctorService.viewProfile(doctor);
		Doctor doctorView = service.findDoctorByUserid(doctor.getUserid());
		doctors = new ArrayList<Doctor>();
		doctors.add(doctorView);
		result.setResult(doctors);

		return result;
	}
	
	
	/**
	 * 
	 * @param Doctor
	 * @return SUCCESS / FAILED
	 */
	@JsonView(Views.Public.class)
	@RequestMapping(value = "/doctor/api/editProfile")
	public DoctorResponseBody editProfile(@RequestBody Doctor doctor) {
		DoctorResponseBody result = new DoctorResponseBody();
		result.setCode("200");
		String doctorCreatedMessage = service.updateDoctor(doctor);
		result.setMsg(doctorCreatedMessage);
		return result;
	}
	
	
	/**
	 * 
	 * @param Doctor
	 * @return SUCCESS / FAILED
	 */
	@JsonView(Views.Public.class)
	@RequestMapping(value = "/doctor/api/increaseLikes")
	public DoctorResponseBody increaseLikes(@RequestBody Doctor doctor) {
		DoctorResponseBody result = new DoctorResponseBody();
		result.setCode("200");
		logger.log(Level.INFO, "increaseLikes Doctor: "+doctor);
//		String doctorCreatedMessage = service.updateDoctor(doctor);
		service.increaseLikes(doctor.getUserid());
		result.setMsg("SUCCESS");
		return result;
	}
	
	// @ResponseBody, not necessary, since class is annotated with @RestController
	// @RequestBody - Convert the json data into object (SearchCriteria) mapped by field name.
	// @JsonView(Views.Public.class) - Optional, limited the json data display to client.
	@JsonView(Views.Public.class)
	@RequestMapping(value = "/doctor/api/search")
	public DoctorResponseBody searchDoctors(@RequestBody DoctorSearchCriterea criterea) {
		logger.log(Level.INFO, "search Doctor: "+criterea);
		DoctorResponseBody result = new DoctorResponseBody();
		
		if(criterea.isEmpty()){
			result.setCode("200");
			result.setCode("Null criterea. No objects present in criterea. recieved : "+criterea.toString());
			return result;
		}
		
		
		List<Doctor> findAllDoctors = service.searchDoctors(criterea);
		result.setResult(findAllDoctors);
		
		//AjaxResponseBody will be converted into json format and send back to client.
		return result;
		
	}
	
	// @ResponseBody, not necessary, since class is annotated with @RestController
	// @RequestBody - Convert the json data into object (SearchCriteria) mapped by field name.
	// @JsonView(Views.Public.class) - Optional, limited the json data display to client.
	@JsonView(Views.Public.class)
	@RequestMapping(value = "/doctor/api/getTreatments")
	public List<String> getTreatments() {
		logger.log(Level.INFO, "search Doctor: getTreatments() ");
		DoctorResponseBody result = new DoctorResponseBody();

		List<String> findAllDoctors = service.getTreatments();
		// result.setResult(findAllDoctors);
		
		//AjaxResponseBody will be converted into json format and send back to client.
		return findAllDoctors;
		
	}
	

	// Init some doctors for testing
	@PostConstruct
	private void iniDataForTesting() {
		doctors = new ArrayList<Doctor>();

	}

}
