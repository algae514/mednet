package com.adamglobal.web.controller;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.validation.Valid;

import org.hibernate.engine.jdbc.spi.SqlExceptionHelper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.util.FileCopyUtils;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.multipart.MultipartFile;

import com.adamglobal.services.DoctorService;
import com.adamglobal.services.ExcelService;
import com.adamglobal.validator.FileValidator;
import com.adamglobal.validator.MultiFileValidator;
import com.adamglobal.web.model.Doctor;
import com.adamglobal.web.model.FileBucket;
import com.adamglobal.web.model.MultiFileBucket;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;

@CrossOrigin(maxAge = 3600)
@Controller
public class FileUploadController {

	private static String UPLOAD_LOCATION = "/apps/uploadedFiles/";
	
	ObjectMapper jsonObjectMapper = new ObjectMapper();
	
	
	SqlExceptionHelper sqlExceptionHelper = new SqlExceptionHelper();
	

	@Autowired
	FileValidator fileValidator;
	
	
	@Autowired
	ExcelService excelService;
	
	@Autowired
	DoctorService doctorService;

	@Autowired
	MultiFileValidator multiFileValidator;

	@InitBinder("fileBucket")
	protected void initBinderFileBucket(WebDataBinder binder) {
		binder.setValidator(fileValidator);
	}

	@InitBinder("multiFileBucket")
	protected void initBinderMultiFileBucket(WebDataBinder binder) {
		binder.setValidator(multiFileValidator);
	}

	@RequestMapping(value = { "/", "/welcome" }, method = RequestMethod.GET)
	public String getHomePage(ModelMap model) {
		return "welcome";
	}

	@RequestMapping(value = "/singleUpload", method = RequestMethod.GET)
	public String getSingleUploadPage(ModelMap model) {
		FileBucket fileModel = new FileBucket();
		model.addAttribute("fileBucket", fileModel);
		return "singleFileUploader";
	}

	@RequestMapping(value = "/singleUpload", method = RequestMethod.POST)
	public List<String> singleFileUpload(@Valid FileBucket fileBucket, BindingResult result, ModelMap model)
			throws IOException {
		
		System.out.println(" invoked singleFileUpload  ");
		 {
			System.out.println("Fetching file");
			MultipartFile multipartFile = fileBucket.getFile();
			System.out.println("Fetching file 1 "+fileBucket.getFile().getBytes().length);
			// Now do something with file...
			FileCopyUtils.copy(fileBucket.getFile().getBytes(),
					new File(UPLOAD_LOCATION + fileBucket.getFile().getOriginalFilename()));

			String fileName = multipartFile.getOriginalFilename();
			
			JsonNode jsonNode = excelService.getTableAsJson(UPLOAD_LOCATION + fileBucket.getFile().getOriginalFilename());
			System.out.println("  jsonNode "+jsonNode);
			
			List<String> errors = new ArrayList();

			for(JsonNode docNode : jsonNode){
				try{
					Doctor newDocNode = jsonObjectMapper.treeToValue(docNode, Doctor.class);
					doctorService.saveDoctor(newDocNode);
				}
				catch(org.hibernate.exception.ConstraintViolationException e){
					
//					sqlExceptionHelper.convert(e.getSQLException() , "");
					System.out.println("Could not save all users because of "+e.getSQLException().getMessage());
					errors.add(" Failed : "+e.getSQLException().getMessage());
					e.printStackTrace();
					continue;
				}
				catch(Exception e){
					System.out.println(" doctorService.save failed "+e.getMessage());
					e.printStackTrace();
				}
				errors.add(" Success: "+docNode.get("userid"));
			}
			
			model.addAttribute("errorMessage", errors);
			model.addAttribute("fileName", fileName);
			return errors;
		}
	}

	@RequestMapping(value = "/multiUpload", method = RequestMethod.GET)
	public String getMultiUploadPage(ModelMap model) {
		MultiFileBucket filesModel = new MultiFileBucket();
		model.addAttribute("multiFileBucket", filesModel);
		return "multiFileUploader";
	}

	@RequestMapping(value = "/multiUpload", method = RequestMethod.POST)
	public String multiFileUpload(@Valid MultiFileBucket multiFileBucket, BindingResult result, ModelMap model)
			throws IOException {

		if (result.hasErrors()) {
			System.out.println("validation errors in multi upload");
			return "multiFileUploader";
		} else {
			System.out.println("Fetching files");
			List<String> fileNames = new ArrayList<String>();

			// Now do something with file...
			for (FileBucket bucket : multiFileBucket.getFiles()) {
				FileCopyUtils.copy(bucket.getFile().getBytes(),
						new File(UPLOAD_LOCATION + bucket.getFile().getOriginalFilename()));
				fileNames.add(bucket.getFile().getOriginalFilename());
			}

			model.addAttribute("fileNames", fileNames);
			return "multiSuccess";
		}
	}

}
