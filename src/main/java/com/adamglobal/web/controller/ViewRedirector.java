package com.adamglobal.web.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.adamglobal.web.model.FileBucket;

@CrossOrigin(maxAge = 3600)
@Controller
public class ViewRedirector {

	@RequestMapping(value = "/", method = RequestMethod.GET)
	public String printWelcome(ModelMap model) {
		return "login";
	}
	
	  @RequestMapping(value = { "/products"}, method = RequestMethod.GET)
	    public String productsPage(ModelMap model) {
	        return "products";
	    }
	  
	  @RequestMapping(value = { "/home"}, method = RequestMethod.GET)
	  public String home(ModelMap model) {
			FileBucket fileModel = new FileBucket();
			model.addAttribute("fileBucket", fileModel);
		  return "home";
	  }
	 
	  @RequestMapping(value = { "/codereport"}, method = RequestMethod.GET)
	  public String codereport() {
		  return "codereport";
	  }
	  
	  

}
