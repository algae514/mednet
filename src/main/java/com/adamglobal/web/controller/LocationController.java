package com.adamglobal.web.controller;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.adamglobal.services.LocationService;
import com.adamglobal.web.jsonview.Views;
import com.adamglobal.web.model.Location;
import com.adamglobal.web.model.LocationResponseBody;
import com.fasterxml.jackson.annotation.JsonView;

@CrossOrigin(maxAge = 3600)
@RestController
public class LocationController {

	List<Location> locations;
	
	Logger logger = Logger.getLogger(LocationController.class.getName());
	
	@Autowired
	LocationService service;
	 
	     
	
	

	
	
	
	// @ResponseBody, not necessary, since class is annotated with @RestController
	// @RequestBody - Convert the json data into object (SearchCriteria) mapped by field name.
	// @JsonView(Views.Public.class) - Optional, limited the json data display to client.
	@JsonView(Views.Public.class)
	@RequestMapping(value = "/doctor/api/getSupportedCities")
	public LocationResponseBody getSupportedCities() {
//		logger.log(Level.INFO, "search Location: "+criterea);
		LocationResponseBody result = new LocationResponseBody();
		List<Location> findAllLocations = service.getSupportedCities();
		result.setResult(findAllLocations);
		//AjaxResponseBody will be converted into json format and send back to client.
		return result;
		
	}

	// @ResponseBody, not necessary, since class is annotated with @RestController
	// @RequestBody - Convert the json data into object (SearchCriteria) mapped by field name.
	// @JsonView(Views.Public.class) - Optional, limited the json data display to client.
	@JsonView(Views.Public.class)
	@CrossOrigin(maxAge = 3600)
	@RequestMapping(value = "/doctor/api/getSupportedLocations")
	public LocationResponseBody getSupportedLocations(@RequestBody Location city) {
		logger.log(Level.INFO, "search Location: ");
		LocationResponseBody result = new LocationResponseBody();
		List<Location> findAllLocations = service.getSupportedLocations(city.getCt());
		result.setResult(findAllLocations);
		//AjaxResponseBody will be converted into json format and send back to client.
		return result;
		
	}
	
	// @ResponseBody, not necessary, since class is annotated with @RestController
	// @RequestBody - Convert the json data into object (SearchCriteria) mapped by field name.
	// @JsonView(Views.Public.class) - Optional, limited the json data display to client.
	@JsonView(Views.Public.class)
	@CrossOrigin(maxAge = 3600)
	@RequestMapping(value = "/doctor/api/createLocations")
	public LocationResponseBody createLocations(@RequestBody Location city) {
		logger.log(Level.INFO, "search Location: "+city);
		LocationResponseBody result = new LocationResponseBody();
		service.saveLocation(city);
		result.setCode("200");
		//AjaxResponseBody will be converted into json format and send back to client.
		return result;
		
	}
	
	
	// @ResponseBody, not necessary, since class is annotated with @RestController
	// @RequestBody - Convert the json data into object (SearchCriteria) mapped by field name.
	// @JsonView(Views.Public.class) - Optional, limited the json data display to client.
	@JsonView(Views.Public.class)
	@CrossOrigin(maxAge = 3600)
	@RequestMapping(value = "/doctor/api/updateLocations")
	public LocationResponseBody updateLocations(Location city) {
		logger.log(Level.INFO, "search Location: ");
		LocationResponseBody result = new LocationResponseBody();
		service.updateLocation(city);
		result.setCode("200");
		//AjaxResponseBody will be converted into json format and send back to client.
		return result;
		
	}
	
	

	// Init some locations for testing
	@PostConstruct
	private void iniDataForTesting() {
		locations = new ArrayList<Location>();

	}

}
