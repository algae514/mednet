package com.adamglobal.web.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import com.adamglobal.web.jsonview.Views;
import com.fasterxml.jackson.annotation.JsonView;

@Entity
@Table(name = "dctr")
public class Doctor {
	
	@Id
	@JsonView(Views.Public.class)
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int id;

	@Column(name = "name", nullable = false)
	@JsonView(Views.Public.class)
    private String name;
    
	@Column(name = "userid", nullable = false)
	@JsonView(Views.Public.class)
    private String userid;
    
	
    @Column(name = "password", nullable = false)
    private String password;
    
    @Column(name = "image")
    @JsonView(Views.Public.class)
    private String image;
    
    @Column(name = "qualfcn")
    @JsonView(Views.Public.class)
    private String qualfcn;

    @Column(name = "issue")
    @JsonView(Views.Public.class)
    private String issue;
    
    @Column(name = "price")
    @JsonView(Views.Public.class)
    private String price;
    
    @Column(name = "ct")
    @JsonView(Views.Public.class)
    private String ct;
    
    @Column(name = "lcn")
    @JsonView(Views.Public.class)
    private String lcn;
    
    @Column(name = "trtmnt")
    @JsonView(Views.Public.class)
    private String trtmnt;

    @Column(name = "avlblTime")
    @JsonView(Views.Public.class)
    private String avlblTime;
    
    @Column(name = "rcmnds")
    @JsonView(Views.Public.class)
    private int rcmnds;
    
    
	public int getRcmnds() {
		return rcmnds;
	}

	public void setRcmnds(int rcmnds) {
		this.rcmnds = rcmnds;
	}

	public String getAvlblTime() {
		return avlblTime;
	}

	public void setAvlblTime(String avlblTime) {
		this.avlblTime = avlblTime;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}


	public String getUserid() {
		return userid;
	}

	public void setUserid(String userid) {
		this.userid = userid;
	}

	public String getCt() {
		return ct;
	}

	public void setCt(String ct) {
		this.ct = ct;
	}

	public String getLcn() {
		return lcn;
	}

	public void setLcn(String lcn) {
		this.lcn = lcn;
	}

	public String getTrtmnt() {
		return trtmnt;
	}

	public void setTrtmnt(String trtmnt) {
		this.trtmnt = trtmnt;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getImage() {
		return image;
	}

	public void setImage(String image) {
		this.image = image;
	}

	public String getQualfcn() {
		return qualfcn;
	}

	public void setQualfcn(String qualfcn) {
		this.qualfcn = qualfcn;
	}

	public String getIssue() {
		return issue;
	}

	public void setIssue(String issue) {
		this.issue = issue;
	}

	public String getPrice() {
		return price;
	}

	public void setPrice(String price) {
		this.price = price;
	}

	
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((avlblTime == null) ? 0 : avlblTime.hashCode());
		result = prime * result + ((ct == null) ? 0 : ct.hashCode());
		result = prime * result + id;
		result = prime * result + ((image == null) ? 0 : image.hashCode());
		result = prime * result + ((issue == null) ? 0 : issue.hashCode());
		result = prime * result + ((lcn == null) ? 0 : lcn.hashCode());
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		result = prime * result + ((password == null) ? 0 : password.hashCode());
		result = prime * result + ((price == null) ? 0 : price.hashCode());
		result = prime * result + ((qualfcn == null) ? 0 : qualfcn.hashCode());
		result = prime * result + ((trtmnt == null) ? 0 : trtmnt.hashCode());
		result = prime * result + ((userid == null) ? 0 : userid.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Doctor other = (Doctor) obj;
		if (avlblTime == null) {
			if (other.avlblTime != null)
				return false;
		} else if (!avlblTime.equals(other.avlblTime))
			return false;
		if (ct == null) {
			if (other.ct != null)
				return false;
		} else if (!ct.equals(other.ct))
			return false;
		if (id != other.id)
			return false;
		if (image == null) {
			if (other.image != null)
				return false;
		} else if (!image.equals(other.image))
			return false;
		if (issue == null) {
			if (other.issue != null)
				return false;
		} else if (!issue.equals(other.issue))
			return false;
		if (lcn == null) {
			if (other.lcn != null)
				return false;
		} else if (!lcn.equals(other.lcn))
			return false;
		if (name == null) {
			if (other.name != null)
				return false;
		} else if (!name.equals(other.name))
			return false;
		if (password == null) {
			if (other.password != null)
				return false;
		} else if (!password.equals(other.password))
			return false;
		if (price == null) {
			if (other.price != null)
				return false;
		} else if (!price.equals(other.price))
			return false;
		if (qualfcn == null) {
			if (other.qualfcn != null)
				return false;
		} else if (!qualfcn.equals(other.qualfcn))
			return false;
		if (trtmnt == null) {
			if (other.trtmnt != null)
				return false;
		} else if (!trtmnt.equals(other.trtmnt))
			return false;
		if (userid == null) {
			if (other.userid != null)
				return false;
		} else if (!userid.equals(other.userid))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "Doctor [id=" + id + ", name=" + name + ", userid=" + userid + ", password=" + password + ", image="
				+ image + ", qualfcn=" + qualfcn + ", issue=" + issue + ", price=" + price + ", ct=" + ct + ", lcn="
				+ lcn + ", trtmnt=" + trtmnt + ", avlblTime=" + avlblTime + ", rcmnds=" + rcmnds + "]";
	}

    

}
