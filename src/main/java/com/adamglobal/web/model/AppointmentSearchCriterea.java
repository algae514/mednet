package com.adamglobal.web.model;

import java.util.Objects;

public class AppointmentSearchCriterea {

    private String ct;
    private String lcn;
    private String trtmnt;
    private String name;
    
    
	public String getCt() {
		return ct;
	}
	public void setCt(String ct) {
		this.ct = ct;
	}
	public String getLcn() {
		return lcn;
	}
	public void setLcn(String lcn) {
		this.lcn = lcn;
	}
	public String getTrtmnt() {
		return trtmnt;
	}
	public void setTrtmnt(String trtmnt) {
		this.trtmnt = trtmnt;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	@Override
	public String toString() {
		return "DoctorSearchCriterea [ct=" + ct + ", lcn=" + lcn + ", trtmnt=" + trtmnt + ", name=" + name + "]";
	}
	
	
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((ct == null) ? 0 : ct.hashCode());
		result = prime * result + ((lcn == null) ? 0 : lcn.hashCode());
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		result = prime * result + ((trtmnt == null) ? 0 : trtmnt.hashCode());
		return result;
	}
	
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		AppointmentSearchCriterea other = (AppointmentSearchCriterea) obj;
		if (ct == null) {
			if (other.ct != null)
				return false;
		} else if (!ct.equals(other.ct))
			return false;
		if (lcn == null) {
			if (other.lcn != null)
				return false;
		} else if (!lcn.equals(other.lcn))
			return false;
		if (name == null) {
			if (other.name != null)
				return false;
		} else if (!name.equals(other.name))
			return false;
		if (trtmnt == null) {
			if (other.trtmnt != null)
				return false;
		} else if (!trtmnt.equals(other.trtmnt))
			return false;
		return true;
	}
	
	public boolean isEmpty() {
		// TODO Auto-generated method stub
		if(ct == null && lcn == null && trtmnt == null && name == null){
			return true;
		}
		return false;
	}
	
	
}
