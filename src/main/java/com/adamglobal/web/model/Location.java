package com.adamglobal.web.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import com.adamglobal.web.jsonview.Views;
import com.fasterxml.jackson.annotation.JsonView;

@Entity
@Table(name = "lcn")
public class Location {
	
	@Id
	@JsonView(Views.Public.class)
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int id;

	@Column(name = "ct", nullable = false)
	@JsonView(Views.Public.class)
    private String ct;
    
	@Column(name = "lcn", nullable = false)
	@JsonView(Views.Public.class)
	private String lcn;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getCt() {
		return ct;
	}

	public void setCt(String city) {
		this.ct = city;
	}

	public String getLcn() {
		return lcn;
	}

	public void setLctn(String location) {
		this.lcn = location;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((ct == null) ? 0 : ct.hashCode());
		result = prime * result + id;
		result = prime * result + ((lcn == null) ? 0 : lcn.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Location other = (Location) obj;
		if (ct == null) {
			if (other.ct != null)
				return false;
		} else if (!ct.equals(other.ct))
			return false;
		if (id != other.id)
			return false;
		if (lcn == null) {
			if (other.lcn != null)
				return false;
		} else if (!lcn.equals(other.lcn))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "Location [id=" + id + ", city=" + ct + ", location=" + lcn + "]";
	}
	
    
}
