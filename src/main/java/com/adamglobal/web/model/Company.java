package com.adamglobal.web.model;

import com.adamglobal.web.jsonview.Views;
import com.fasterxml.jackson.annotation.JsonView;

import java.math.BigDecimal;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.Digits;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import org.hibernate.annotations.Type;
import org.hibernate.validator.constraints.NotEmpty;
import org.joda.time.LocalDate;
import org.springframework.format.annotation.DateTimeFormat;

@Entity
@Table(name = "COMPANY")
public class Company {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int id;

    @Column(name = "NAME", nullable = false)
    private String name;

    @NotNull
    @DateTimeFormat(pattern="dd/MM/yyyy") 
    @Column(name = "ESTABLISHED_DATE", nullable = false)
    @Type(type="org.jadira.usertype.dateandtime.joda.PersistentLocalDate")
    private LocalDate establishedDate;
 
    
    @NotEmpty
    @Column(name = "USERNAME", unique=true, nullable = false)
    @JsonView(Views.Public.class)
    private String username;
    
    @Column(name = "PASSWORD", unique=true, nullable = false)
    private String password;
    
    @Column(name = "EMAIL", unique=true, nullable = false)
	@JsonView(Views.Public.class)
    private String email;
    
    @Column(name = "PHONE", unique=true, nullable = false)
	@JsonView(Views.Public.class)
    private String phone;
    
    @Column(name = "ADDRESS", unique=true, nullable = false)
    @JsonView(Views.Public.class)
    private String address;
    
    
    @Column(name = "DESCRIPTION", unique=true, nullable = false)
    @JsonView(Views.Public.class)
    private String description;
    
    
    @Column(name = "LOGO", unique=true, nullable = false)
    @JsonView(Views.Public.class)
    private String logo;
    
    
    @Column(name = "TAG_LINE", unique=true, nullable = false)
    @JsonView(Views.Public.class)
    private String tagLine;
    
    @Column(name = "WEBSITE", unique=true, nullable = false)
    @JsonView(Views.Public.class)
    private String website;
    
	
    public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}



	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}
	
	

	public LocalDate getEstablishedDate() {
		return establishedDate;
	}

	public void setEstablishedDate(LocalDate establishedDate) {
		this.establishedDate = establishedDate;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getLogo() {
		return logo;
	}

	public void setLogo(String logo) {
		this.logo = logo;
	}

	public String getTagLine() {
		return tagLine;
	}

	public void setTagLine(String tagLine) {
		this.tagLine = tagLine;
	}

	public String getWebsite() {
		return website;
	}

	public void setWebsite(String website) {
		this.website = website;
	}

	@Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + id;
        result = prime * result + ((username == null) ? 0 : username.hashCode());
        return result;
    }
 
    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (!(obj instanceof Company))
            return false;
        Company other = (Company) obj;
        if (id != other.id)
            return false;
        if (username == null) {
            if (other.username!= null)
                return false;
        } else if (!username.equals(other.username))
            return false;
        return true;
    }

	@Override
	public String toString() {
		return "Company [id=" + id + ", name=" + name + ", establishedDate=" + establishedDate + ", username="
				+ username + ", password=" + password + ", email=" + email + ", phone=" + phone + ", address=" + address
				+ ", description=" + description + ", logo=" + logo + ", tagLine=" + tagLine + ", website=" + website
				+ "]";
	}

    

}
