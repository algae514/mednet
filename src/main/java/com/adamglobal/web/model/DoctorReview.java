package com.adamglobal.web.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "dctr_rvw")
public class DoctorReview {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int id;
	

    @Column(name = "dctr_id", nullable = false)
    private int dctrId;

    @Column(name = "ptnt_id", nullable = false)
    private int ptntId;
    
    @Column(name = "bhvr")
    private int bhvr;
    
    
    @Column(name = "amb")
    private int amb;
    
    
    @Column(name = "overall")
    private int overall;
    
    @Column(name = "fdbk")
    private String fdbk;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getDctrId() {
		return dctrId;
	}

	public void setDctrId(int dctrId) {
		this.dctrId = dctrId;
	}

	public int getPtntId() {
		return ptntId;
	}

	public void setPtntId(int ptntId) {
		this.ptntId = ptntId;
	}

	public int getBhvr() {
		return bhvr;
	}

	public void setBhvr(int bhvr) {
		this.bhvr = bhvr;
	}

	public int getAmb() {
		return amb;
	}

	public void setAmb(int amb) {
		this.amb = amb;
	}

	public int getOverall() {
		return overall;
	}

	public void setOverall(int overall) {
		this.overall = overall;
	}

	public String getFdbk() {
		return fdbk;
	}

	public void setFdbk(String fdbk) {
		this.fdbk = fdbk;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + amb;
		result = prime * result + bhvr;
		result = prime * result + dctrId;
		result = prime * result + ((fdbk == null) ? 0 : fdbk.hashCode());
		result = prime * result + id;
		result = prime * result + overall;
		result = prime * result + ptntId;
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		DoctorReview other = (DoctorReview) obj;
		if (amb != other.amb)
			return false;
		if (bhvr != other.bhvr)
			return false;
		if (dctrId != other.dctrId)
			return false;
		if (fdbk == null) {
			if (other.fdbk != null)
				return false;
		} else if (!fdbk.equals(other.fdbk))
			return false;
		if (id != other.id)
			return false;
		if (overall != other.overall)
			return false;
		if (ptntId != other.ptntId)
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "DoctorReview [id=" + id + ", dctrId=" + dctrId + ", ptntId=" + ptntId + ", bhvr=" + bhvr + ", amb="
				+ amb + ", overall=" + overall + ", fdbk=" + fdbk + "]";
	}
    
}
