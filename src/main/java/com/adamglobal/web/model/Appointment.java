package com.adamglobal.web.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

import org.hibernate.annotations.Type;
import org.joda.time.LocalDateTime;
import org.springframework.format.annotation.DateTimeFormat;

import com.adamglobal.web.jsonview.Views;
import com.fasterxml.jackson.annotation.JsonView;

@Entity
@Table(name = "APTMNT")
public class Appointment {
	
	@Id
	@JsonView(Views.Public.class)
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int id;

	@JsonView(Views.Public.class)
    @Column(name = "dctr_id", nullable = false)
    private int dctrId;

	@JsonView(Views.Public.class)
    @Column(name = "ptnt_id", nullable = false)
    private int ptntId;
    
    
    @NotNull
    @DateTimeFormat(pattern="dd/MM/yyyy HH:mm:ss") 
    @Column(name = "from_time", nullable = false)
    @Type(type="org.jadira.usertype.dateandtime.joda.PersistentLocalDateTime")
    @JsonView(Views.Public.class)
    private LocalDateTime fromTime;
    
    
    @NotNull
    @DateTimeFormat(pattern="dd/MM/yyyy HH:mm:ss") 
    @Column(name = "to_time", nullable = false)
    @Type(type="org.jadira.usertype.dateandtime.joda.PersistentLocalDateTime")
    @JsonView(Views.Public.class)
    private LocalDateTime toTime;
    
    
    
	public int getId() {
		return id;
	}



	public void setId(int id) {
		this.id = id;
	}



	public int getDctrId() {
		return dctrId;
	}



	public void setDctrId(int dctrId) {
		this.dctrId = dctrId;
	}



	public int getPtntId() {
		return ptntId;
	}



	public void setPtntId(int ptntId) {
		this.ptntId = ptntId;
	}



	public LocalDateTime getFromTime() {
		return fromTime;
	}



	public void setFromTime(LocalDateTime fromTime) {
		this.fromTime = fromTime;
	}



	public LocalDateTime getToTime() {
		return toTime;
	}



	public void setToTime(LocalDateTime toTime) {
		this.toTime = toTime;
	}



	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + dctrId;
		result = prime * result + ((fromTime == null) ? 0 : fromTime.hashCode());
		result = prime * result + id;
		result = prime * result + ptntId;
		result = prime * result + ((toTime == null) ? 0 : toTime.hashCode());
		return result;
	}



	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Appointment other = (Appointment) obj;
		if (dctrId != other.dctrId)
			return false;
		if (fromTime == null) {
			if (other.fromTime != null)
				return false;
		} else if (!fromTime.equals(other.fromTime))
			return false;
		if (id != other.id)
			return false;
		if (ptntId != other.ptntId)
			return false;
		if (toTime == null) {
			if (other.toTime != null)
				return false;
		} else if (!toTime.equals(other.toTime))
			return false;
		return true;
	}



	@Override
	public String toString() {
		return "Appointment [id=" + id + ", dctrId=" + dctrId + ", ptntId=" + ptntId + ", fromTime=" + fromTime
				+ ", toTime=" + toTime + "]";
	}



}
