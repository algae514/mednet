package com.adamglobal.dao;

import java.lang.reflect.Field;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.hibernate.Criteria;
import org.hibernate.Query;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;

import com.adamglobal.web.model.Appointment;
import com.adamglobal.web.model.AppointmentSearchCriterea;
import com.adamglobal.web.model.User;

@Repository("AppointmentDao")
public class AppointmentDaoImpl extends AbstractDao<Integer, Appointment>implements AppointmentDao {

	Logger logger = Logger.getLogger(AppointmentDaoImpl.class.getName());

	public Appointment findById(int id) {
		return getByKey(id);
	}

	public void saveAppointment(Appointment Appointment) {
		persist(Appointment);
	}

	public void updateAppointment(Appointment Appointment) {
		update(Appointment);
	}

	public void deleteAppointmentByUserId(String userId) {
		Query query = getSession().createSQLQuery("delete from Appointment where userId = :userId");
		query.setString("userid", userId);
		query.executeUpdate();
	}

	@SuppressWarnings("unchecked")
	public List<Appointment> findAllAppointments() {
		Criteria criteria = createEntityCriteria();
		return (List<Appointment>) criteria.list();
	}

	public Appointment findAppointmentByUserId(String userId) {
		Criteria criteria = createEntityCriteria();
		criteria.add(Restrictions.eq("userid", userId));
		return (Appointment) criteria.uniqueResult();
	}

	@Override
	public List<Appointment> searchAppointments(AppointmentSearchCriterea appointment) {
		// TODO Auto-generated method stub
		Criteria criteria = createEntityCriteria();
		criteria = buildCritereaOnAllObject(criteria, appointment);
		return (List<Appointment>) criteria.list();
	}

	private Criteria buildCritereaOnAllObject(Criteria criteria, AppointmentSearchCriterea appointment_criterea) {
		
		if (appointment_criterea.getLcn() != null && appointment_criterea.getLcn().trim().length() != 0) {
			criteria.add(Restrictions.ilike("lcn", "%"+appointment_criterea.getLcn()+"%"));
		}
		if (appointment_criterea.getTrtmnt() != null && appointment_criterea.getTrtmnt().trim().length() !=0 ) {
			criteria.add(Restrictions.ilike("trtmnt", "%"+appointment_criterea.getTrtmnt()+"%"));
		}
		if (appointment_criterea.getCt() != null && appointment_criterea.getCt().trim().length()!=0 ) {
			criteria.add(Restrictions.ilike("ct", "%"+appointment_criterea.getCt()+"%"));
		}
		if (appointment_criterea.getName() != null && appointment_criterea.getName().trim().length() != 0  ) {
			criteria.add(Restrictions.ilike("name", "%"+appointment_criterea.getName()+"%"));
		}

		return criteria;
	}

	@Override
	public void deleteById(int id) {
		// TODO Auto-generated method stub
		Query query = getSession().createSQLQuery("delete from Appointment where id = :id");
		query.setInteger("id", id);
		query.executeUpdate();
	}

	@Override
	public boolean isAppointmentUserIdUnique(String userId) {
		// TODO Auto-generated method stub
		Criteria criteria = createEntityCriteria();
		criteria.add(Restrictions.eq("userid", userId));
		return criteria.uniqueResult() == null ? true : false;
	}

	@Override
	public Appointment searchAppointmentsAuth(Appointment appointment) {
		// TODO Auto-generated method stub

		Criteria criteria = createEntityCriteria();

		logger.log(Level.INFO, "Login Appointment: input provided : " + appointment);
		criteria.add(Restrictions.eq("dctr_id", appointment.getDctrId()));
		criteria.add(Restrictions.eq("ptnt_id", appointment.getPtntId()));
		return (Appointment) criteria.uniqueResult();

	}

}