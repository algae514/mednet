package com.adamglobal.dao;

import java.util.List;

import com.adamglobal.web.model.Doctor;
import com.adamglobal.web.model.DoctorSearchCriterea;
 
 
public interface DoctorDao {
 
	Doctor findById(int id);

	void saveDoctor(Doctor Doctor);

	void updateDoctor(Doctor Doctor);

	void deleteById(int ssn);

	List<Doctor> findAllDoctors();

	Doctor findDoctorByUserId(String userId);

	boolean isDoctorUserIdUnique(String userId);

	List<Doctor> searchDoctors(DoctorSearchCriterea criterea);

	List<String> getTreatments();

	Doctor searchDoctorsAuth(Doctor doctor);
 
}