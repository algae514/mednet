package com.adamglobal.dao;

import java.util.List;

import com.adamglobal.web.model.Patient;
import com.adamglobal.web.model.PatientSearchCriterea;
 
 
public interface PatientDao {
 
	Patient findById(int id);

	void savePatient(Patient Patient);

	void updatePatient(Patient Patient);

	void deleteById(int ssn);

	List<Patient> findAllPatients();

	Patient findPatientByUserId(String userId);

	boolean isPatientUserIdUnique(String userId);

	List<Patient> searchPatients(PatientSearchCriterea criterea);

	Patient searchPatientsAuth(Patient patient);
 
}