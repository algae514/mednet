package com.adamglobal.dao;

import java.util.List;

import com.adamglobal.web.model.Location;

 
 
public interface LocationDao {
 
	Location findById(int id);

	void saveLocation(Location Location);

	void updateLocation(Location Location);

	void deleteById(int ssn);

	List<Location> findAllLocations();

	Location findLocationByUserId(String userId);

	boolean isLocationUserIdUnique(String userId);


	List<Location> getSupportedCities();

	List<Location> getSupportedLocations(String city);
 
}