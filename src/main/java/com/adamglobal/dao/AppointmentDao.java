package com.adamglobal.dao;

import java.util.List;

import com.adamglobal.web.model.Appointment;
import com.adamglobal.web.model.AppointmentSearchCriterea;
 
 
public interface AppointmentDao {
 
	Appointment findById(int id);

	void saveAppointment(Appointment Appointment);

	void updateAppointment(Appointment Appointment);

	void deleteById(int ssn);

	List<Appointment> findAllAppointments();

	Appointment findAppointmentByUserId(String userId);

	boolean isAppointmentUserIdUnique(String userId);

	List<Appointment> searchAppointments(AppointmentSearchCriterea criterea);

	Appointment searchAppointmentsAuth(Appointment appointment);
 
}