package com.adamglobal.dao;

import java.lang.reflect.Field;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.hibernate.Criteria;
import org.hibernate.Query;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;

import com.adamglobal.web.controller.LocationController;
import com.adamglobal.web.model.Location;
import com.adamglobal.web.model.User;

@Repository("LocationDao")
public class LocationDaoImpl extends AbstractDao<Integer, Location>implements LocationDao {

	Logger logger = Logger.getLogger(LocationDaoImpl.class.getName());

	public Location findById(int id) {
		return getByKey(id);
	}

	public void saveLocation(Location Location) {
		persist(Location);
	}

	public void updateLocation(Location Location) {
		update(Location);
	}

	public void deleteLocationByUserId(String userId) {
		Query query = getSession().createSQLQuery("delete from Location where userId = :userId");
		query.setString("userid", userId);
		query.executeUpdate();
	}

	@SuppressWarnings("unchecked")
	public List<Location> findAllLocations() {
		Criteria criteria = createEntityCriteria();
		return (List<Location>) criteria.list();
	}

	public Location findLocationByUserId(String userId) {
		Criteria criteria = createEntityCriteria();
		criteria.add(Restrictions.eq("userid", userId));
		return (Location) criteria.uniqueResult();
	}


	@Override
	public void deleteById(int id) {
		// TODO Auto-generated method stub
		Query query = getSession().createSQLQuery("delete from Location where id = :id");
		query.setInteger("id", id);
		query.executeUpdate();
	}

	@Override
	public boolean isLocationUserIdUnique(String userId) {
		// TODO Auto-generated method stub
		Criteria criteria = createEntityCriteria();
		criteria.add(Restrictions.eq("userid", userId));
		return criteria.uniqueResult() == null ? true : false;
	}


	@Override
	public List<Location> getSupportedCities() {
		// TODO Auto-generated method stub
		Criteria criteria = createEntityCriteria();
		return criteria.list();
	}

	@Override
	public List<Location> getSupportedLocations(String city) {
		Criteria criteria = createEntityCriteria();
		criteria.add(Restrictions.eq("ct", city));
		return criteria.list();
	}

}