package com.adamglobal.dao;

import java.lang.reflect.Field;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.hibernate.Criteria;
import org.hibernate.Query;
import org.hibernate.criterion.Restrictions;
import org.hibernate.criterion.Projections;
import org.springframework.stereotype.Repository;

import com.adamglobal.web.controller.DoctorController;
import com.adamglobal.web.model.Doctor;
import com.adamglobal.web.model.DoctorSearchCriterea;
import com.adamglobal.web.model.User;

@Repository("DoctorDao")
public class DoctorDaoImpl extends AbstractDao<Integer, Doctor>implements DoctorDao {

	Logger logger = Logger.getLogger(DoctorDaoImpl.class.getName());

	public Doctor findById(int id) {
		return getByKey(id);
	}

	public void saveDoctor(Doctor Doctor) {
		persist(Doctor);
	}

	public void updateDoctor(Doctor Doctor) {
		update(Doctor);
	}

	public void deleteDoctorByUserId(String userId) {
		Query query = getSession().createSQLQuery("delete from Doctor where userId = :userId");
		query.setString("userid", userId);
		query.executeUpdate();
	}

	@SuppressWarnings("unchecked")
	public List<Doctor> findAllDoctors() {
		Criteria criteria = createEntityCriteria();
		return (List<Doctor>) criteria.list();
	}

	public Doctor findDoctorByUserId(String userId) {
		Criteria criteria = createEntityCriteria();
		criteria.add(Restrictions.eq("userid", userId));
		return (Doctor) criteria.uniqueResult();
	}

	@Override
	public List<Doctor> searchDoctors(DoctorSearchCriterea doctor) {
		// TODO Auto-generated method stub
		Criteria criteria = createEntityCriteria();
		criteria = buildCritereaOnAllObject(criteria, doctor);
		return (List<Doctor>) criteria.list();
	}

	@Override
	public List<String> getTreatments() {
		// TODO Auto-generated method stub
		Criteria criteria = createEntityCriteria().setProjection(Projections.distinct(Projections.property("trtmnt")));
		return (List<String>) criteria.list();
	}

	private Criteria buildCritereaOnAllObject(Criteria criteria, DoctorSearchCriterea doctor_criterea) {
		
		if (doctor_criterea.getLcn() != null && doctor_criterea.getLcn().trim().length() != 0) {
			criteria.add(Restrictions.ilike("lcn", "%"+doctor_criterea.getLcn()+"%"));
		}
		if (doctor_criterea.getTrtmnt() != null && doctor_criterea.getTrtmnt().trim().length() !=0 ) {
			criteria.add(Restrictions.ilike("trtmnt", "%"+doctor_criterea.getTrtmnt()+"%"));
		}
		if (doctor_criterea.getCt() != null && doctor_criterea.getCt().trim().length()!=0 ) {
			criteria.add(Restrictions.ilike("ct", "%"+doctor_criterea.getCt()+"%"));
		}
		if (doctor_criterea.getName() != null && doctor_criterea.getName().trim().length() != 0  ) {
			criteria.add(Restrictions.ilike("name", "%"+doctor_criterea.getName()+"%"));
		}

		return criteria;
	}

	@Override
	public void deleteById(int id) {
		// TODO Auto-generated method stub
		Query query = getSession().createSQLQuery("delete from Doctor where id = :id");
		query.setInteger("id", id);
		query.executeUpdate();
	}

	@Override
	public boolean isDoctorUserIdUnique(String userId) {
		// TODO Auto-generated method stub
		Criteria criteria = createEntityCriteria();
		criteria.add(Restrictions.eq("userid", userId));
		return criteria.uniqueResult() == null ? true : false;
	}

	@Override
	public Doctor searchDoctorsAuth(Doctor doctor) {
		// TODO Auto-generated method stub

		Criteria criteria = createEntityCriteria();

		logger.log(Level.INFO, "Login Doctor: input provided : " + doctor);

		criteria.add(Restrictions.eq("userid", doctor.getUserid()));
		criteria.add(Restrictions.eq("password", doctor.getPassword()));
		return (Doctor) criteria.uniqueResult();

	}

}