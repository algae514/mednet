package com.adamglobal.dao;

import java.lang.reflect.Field;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.hibernate.Criteria;
import org.hibernate.Query;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;

import com.adamglobal.web.controller.PatientController;
import com.adamglobal.web.model.Patient;
import com.adamglobal.web.model.PatientSearchCriterea;
import com.adamglobal.web.model.User;

@Repository("PatientDao")
public class PatientDaoImpl extends AbstractDao<Integer, Patient>implements PatientDao {

	Logger logger = Logger.getLogger(PatientDaoImpl.class.getName());

	public Patient findById(int id) {
		return getByKey(id);
	}

	public void savePatient(Patient Patient) {
		persist(Patient);
	}

	public void updatePatient(Patient Patient) {
		update(Patient);
	}

	public void deletePatientByUserId(String userId) {
		Query query = getSession().createSQLQuery("delete from Patient where userId = :userId");
		query.setString("userid", userId);
		query.executeUpdate();
	}

	@SuppressWarnings("unchecked")
	public List<Patient> findAllPatients() {
		Criteria criteria = createEntityCriteria();
		return (List<Patient>) criteria.list();
	}

	public Patient findPatientByUserId(String userId) {
		Criteria criteria = createEntityCriteria();
		criteria.add(Restrictions.eq("userid", userId));
		return (Patient) criteria.uniqueResult();
	}

	@Override
	public List<Patient> searchPatients(PatientSearchCriterea patient) {
		// TODO Auto-generated method stub
		Criteria criteria = createEntityCriteria();
		criteria = buildCritereaOnAllObject(criteria, patient);
		return (List<Patient>) criteria.list();
	}

	private Criteria buildCritereaOnAllObject(Criteria criteria, PatientSearchCriterea patient_criterea) {
		if (patient_criterea.getLcn() != null) {
			criteria.add(Restrictions.ilike("lcn", patient_criterea.getLcn()));
		}
		if (patient_criterea.getTrtmnt() != null) {
			criteria.add(Restrictions.ilike("trtmnt", patient_criterea.getTrtmnt()));
		}
		if (patient_criterea.getCt() != null) {
			criteria.add(Restrictions.ilike("ct", patient_criterea.getCt()));
		}
		return criteria;
	}

	@Override
	public void deleteById(int id) {
		// TODO Auto-generated method stub
		Query query = getSession().createSQLQuery("delete from Patient where id = :id");
		query.setInteger("id", id);
		query.executeUpdate();
	}

	@Override
	public boolean isPatientUserIdUnique(String userId) {
		// TODO Auto-generated method stub
		Criteria criteria = createEntityCriteria();
		criteria.add(Restrictions.eq("userid", userId));
		return criteria.uniqueResult() == null ? true : false;
	}

	@Override
	public Patient searchPatientsAuth(Patient patient) {
		// TODO Auto-generated method stub

		Criteria criteria = createEntityCriteria();

		logger.log(Level.INFO, "Login Patient: input provided : " + patient);

		criteria.add(Restrictions.eq("userid", patient.getUserid()));
		criteria.add(Restrictions.eq("password", patient.getPassword()));
		return (Patient) criteria.uniqueResult();

	}

}