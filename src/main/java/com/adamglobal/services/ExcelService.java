package com.adamglobal.services;

import java.io.IOException;
import java.util.List;
import java.util.Map;

import org.apache.poi.ss.usermodel.Sheet;

import com.fasterxml.jackson.databind.JsonNode;

public interface ExcelService {

	public List<Map<String, String>> getTableAsListMap(String excelSheetLocation) throws IOException;
	
	public JsonNode getTableAsJson(String excelSheetLocation) throws IOException;
	
	public List<String>  getHeadersOfExcel(Sheet firstSheet);
	
	
	
}
