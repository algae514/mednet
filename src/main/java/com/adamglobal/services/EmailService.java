package com.adamglobal.services;

import com.adamglobal.web.model.Appointment;

public interface EmailService {
	
	public void sendEmail(Appointment appointment);


}
