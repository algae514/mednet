package com.adamglobal.services;

import java.util.List;

import com.adamglobal.web.model.Doctor;
import com.adamglobal.web.model.DoctorSearchCriterea;

public interface DoctorService {


	Doctor findById(int id);

	void saveDoctor(Doctor Doctor);

	String updateDoctor(Doctor Doctor);

	void deleteById(int ssn);

	List<Doctor> findAllDoctors();

	Doctor findDoctorByUserid(String userId);

	boolean isDoctorUseridUnique(String userId);

	List<Doctor> searchDoctors(DoctorSearchCriterea criterea);
	List<String> getTreatments();

	Doctor searchDoctorsAuth(Doctor doctor);

	void increaseLikes(String id);


}
