package com.adamglobal.services;

import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.adamglobal.dao.PatientDao;
import com.adamglobal.web.controller.PatientController;
import com.adamglobal.web.model.Patient;
import com.adamglobal.web.model.PatientSearchCriterea;

@Service("patientService")
@Transactional
public class PatientServiceImpl implements PatientService {

	@Autowired
	private PatientDao dao;

	@Autowired
	MessageSource messageSource;
	Logger logger = Logger.getLogger(PatientServiceImpl.class.getName());
	
	public Patient findById(int id) {
		return dao.findById(id);
	}

	public void savePatient(Patient Patient) {
		dao.savePatient(Patient);
	}

	/*
	 * Since the method is running with Transaction, No need to call hibernate
	 * update explicitly. Just fetch the entity from db and update it with
	 * proper values within transaction. It will be updated in db once
	 * transaction ends.
	 */
	public String updatePatient(Patient patient) {
		
		Patient entity = dao.findPatientByUserId(patient.getUserid());
		System.out.println(" fetched : patient s from DB : :" + entity);

		if (entity != null) {
			entity.setUserid(patient.getUserid());
			entity.setName(patient.getName());
			entity.setPassword(patient.getPassword());
			logger.log(Level.INFO, "to be udpated : editProfile Patient: "+entity);
			dao.updatePatient(entity);
			return "SUCCESS";
		}else{
			return "invalid userid";	
		}

	}

	public void deletePatientByUserId(String userId) {
		Patient findPatientByUserId = dao.findPatientByUserId(userId);
		int id = findPatientByUserId.getId();
		deleteById(id);
	}

	public List<Patient> findAllPatients() {
		return dao.findAllPatients();
	}

	public Patient findPatientByUserId(String userId) {
		return dao.findPatientByUserId(userId);
	}

	public boolean isPatientUserIdUnique(Integer id, String userId) {
		Patient Patient = findPatientByUserId(userId);
		return (Patient == null || ((id != null) && (Patient.getId() == id)));
	}

	@Override
	public List<Patient> searchPatients(PatientSearchCriterea criterea) {
		// TODO Auto-generated method stub
		return dao.searchPatients(criterea);
	}

	@Override
	public void deleteById(int ssn) {
		// TODO Auto-generated method stub
		dao.deleteById(ssn);

	}

	@Override
	public Patient findPatientByUserid(String userId) {
		// TODO Auto-generated method stub
		return dao.findPatientByUserId(userId);
	}

	@Override
	public boolean isPatientUseridUnique(String userId) {
		// TODO Auto-generated method stub
		Patient findPatientByUserId = dao.findPatientByUserId(userId);
		return findPatientByUserId == null ? true : false;
	}

	@Override
	public Patient searchPatientsAuth(Patient patient) {
		// TODO Auto-generated method stub
		return dao.searchPatientsAuth(patient);
	}

}