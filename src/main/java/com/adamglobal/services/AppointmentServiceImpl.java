package com.adamglobal.services;

import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.adamglobal.dao.AppointmentDao;
import com.adamglobal.web.model.Appointment;
import com.adamglobal.web.model.AppointmentSearchCriterea;

@Service("appointmentService")
@Transactional
public class AppointmentServiceImpl implements AppointmentService {

	@Autowired
	private AppointmentDao dao;

	@Autowired
	MessageSource messageSource;
	Logger logger = Logger.getLogger(AppointmentServiceImpl.class.getName());
	
	public Appointment findById(int id) {
		return dao.findById(id);
	}

	public void saveAppointment(Appointment Appointment) {
		dao.saveAppointment(Appointment);
	}

	/*
	 * Since the method is running with Transaction, No need to call hibernate
	 * update explicitly. Just fetch the entity from db and update it with
	 * proper values within transaction. It will be updated in db once
	 * transaction ends.
	 */
	public String updateAppointment(Appointment appointment) {
		// Appointment entity = dao.findById(Appointment.getId());
		Appointment entity = dao.findById(appointment.getId());
		System.out.println(" fetched : appointment s from DB : :" + entity);

		if (entity != null) {
			entity.setDctrId(appointment.getDctrId());
			entity.setFromTime(appointment.getFromTime());
			entity.setPtntId(appointment.getPtntId());
			entity.setToTime(appointment.getToTime());
			logger.log(Level.INFO, "to be udpated : editProfile Appointment: "+entity);
			dao.updateAppointment(entity);
			return "SUCCESS";
		}else{
			return "invalid userid";	
		}
	}

	public void deleteAppointmentByUserId(String userId) {
		Appointment findAppointmentByUserId = dao.findAppointmentByUserId(userId);
		int id = findAppointmentByUserId.getId();
		deleteById(id);
	}

	public List<Appointment> findAllAppointments() {
		return dao.findAllAppointments();
	}

	public Appointment findAppointmentByUserId(String userId) {
		return dao.findAppointmentByUserId(userId);
	}

	public boolean isAppointmentUserIdUnique(Integer id, String userId) {
		Appointment Appointment = findAppointmentByUserId(userId);
		return (Appointment == null || ((id != null) && (Appointment.getId() == id)));
	}

	@Override
	public List<Appointment> searchAppointments(AppointmentSearchCriterea criterea) {
		// TODO Auto-generated method stub
		return dao.searchAppointments(criterea);
	}

	@Override
	public void deleteById(int ssn) {
		// TODO Auto-generated method stub
		dao.deleteById(ssn);

	}

	@Override
	public Appointment findAppointmentByUserid(String userId) {
		// TODO Auto-generated method stub
		return dao.findAppointmentByUserId(userId);
	}

	@Override
	public boolean isAppointmentUseridUnique(String userId) {
		// TODO Auto-generated method stub
		Appointment findAppointmentByUserId = dao.findAppointmentByUserId(userId);
		return findAppointmentByUserId == null ? true : false;
	}

	@Override
	public Appointment searchAppointmentsAuth(Appointment appointment) {
		// TODO Auto-generated method stub
		return dao.searchAppointmentsAuth(appointment);
	}


}