package com.adamglobal.services;

import javax.mail.Message;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

import org.joda.time.LocalDateTime;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.MailException;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessagePreparator;
import org.springframework.stereotype.Service;

import com.adamglobal.dao.DoctorDao;
import com.adamglobal.dao.PatientDao;
import com.adamglobal.web.model.Appointment;
import com.adamglobal.web.model.Doctor;
import com.adamglobal.web.model.Patient;

@Service("emailService")
public class EmailServiceImpl implements EmailService {

	@Autowired
	JavaMailSender mailSender;

	@Autowired
	private DoctorService docService;

	@Autowired
	private PatientService ptntService;

	@Override
	public void sendEmail(Appointment appointment) {
		
		MimeMessagePreparator doctorPreparator = getDoctorMessagePreparator(appointment);
		MimeMessagePreparator patientPreparator = getPatientMessagePreparator(appointment);
		try {
			mailSender.send(doctorPreparator);
			mailSender.send(patientPreparator);
			
			System.out.println("Message Send...Hurrey");
		} catch (MailException ex) {
			System.err.println(ex.getMessage());
		}
	}

	private MimeMessagePreparator getDoctorMessagePreparator(final Appointment appointment) {

		int dctrId = appointment.getDctrId();
		LocalDateTime fromTime = appointment.getFromTime();
		
		
		
		int ptntId = appointment.getPtntId();
		Patient ptnt = ptntService.findById(ptntId);
		Doctor dctr = docService.findById(dctrId);
		MimeMessagePreparator preparator = new MimeMessagePreparator() {

			public void prepare(MimeMessage mimeMessage) throws Exception {
				mimeMessage.setFrom("balu.in.u@gmail.com");
				mimeMessage.setRecipient(Message.RecipientType.TO, new InternetAddress(dctr.getUserid()));
				mimeMessage.setText("Dear " + dctr.getName() + ",\n\n" + ptnt.getName()
						+ " has booked an appointment at " + appointment.getFromTime().toString("yyyy-MMM-dd hh:mm")
						+ ". \nbelow are details :"
						+ "\nName " + ptnt.getName() + " "
						+ "\nCode " + ptnt.getCd() + " "
								+ "\n\nthank you.");
				mimeMessage.setSubject("Appointment booked");
			}
		};
		return preparator;
	}
	
	private MimeMessagePreparator getPatientMessagePreparator(final Appointment appointment) {
		
		int dctrId = appointment.getDctrId();
		int ptntId = appointment.getPtntId();
		Patient ptnt = ptntService.findById(ptntId);
		Doctor dctr = docService.findById(dctrId);
		MimeMessagePreparator preparator = new MimeMessagePreparator() {
			
			public void prepare(MimeMessage mimeMessage) throws Exception {
				mimeMessage.setFrom("balu.in.u@gmail.com");
				mimeMessage.setRecipient(Message.RecipientType.TO, new InternetAddress(ptnt.getUserid()));
				mimeMessage.setText("Dear " + ptnt.getName() + ",\n\nyour apointment has been booked at " + appointment.getFromTime().toString("yyyy-MMM-dd hh:mm")
				+ ". \nbelow are details "
				+ ":\nuserid " + dctr.getUserid() + ""
				+ ":\nName " + dctr.getName() + ""
				+ " \n\nthank you.");
				mimeMessage.setSubject("Appointment booked");
			}
		};
		return preparator;
	}

}