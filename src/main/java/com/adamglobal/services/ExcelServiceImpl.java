package com.adamglobal.services;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.DataFormatter;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;

@Service("excelService")
public class ExcelServiceImpl implements ExcelService {

	static DataFormatter formatter = new DataFormatter();

	/**
	 * 
	 * @param fileAbsoluteLocation
	 * @return
	 * @throws IOException
	 */
	@Override
	public List<Map<String, String>> getTableAsListMap(String excelSheetLocation) throws IOException {

		FileInputStream inputStream = new FileInputStream(new File(excelSheetLocation));
		Workbook workbook = getWorkbook(inputStream, excelSheetLocation);
		Sheet firstSheet = workbook.getSheetAt(0);
		List<Map<String, String>> stringMap = getTableAsListMap(firstSheet);
		inputStream.close();
		return stringMap;

	}

	private static Workbook getWorkbook(FileInputStream inputStream, String excelFilePath) throws IOException {

		Workbook workbook = null;
		System.out.println(" excelFilePath  " + excelFilePath + "   ens with ? " + excelFilePath.endsWith("xlsx"));

		if (excelFilePath.endsWith("xlsx")) {

			workbook = new XSSFWorkbook(inputStream);

		} else if (excelFilePath.endsWith("xls")) {
			System.out.println(" 2007 + workbook ");
			workbook = new HSSFWorkbook(inputStream);

		}
		return workbook;
	}

	public List<Map<String, String>> getTableAsListMap(Sheet firstSheet) {

		List<Map<String, String>> stringMapList = new ArrayList<Map<String, String>>();

		Iterator<Row> iterator = firstSheet.iterator();
		int counter = 0;

		List<String> headers = new ArrayList<String>();

		while (iterator.hasNext()) {

			Row nextRow = iterator.next();

			if (counter == 0) {
				// handle header
				System.out.println(" Need to take subject names ");
				// Iterator<Cell> cellIterator = nextRow.cellIterator();
				int colCounter = 0;

				for (Iterator iterator1 = nextRow.cellIterator(); iterator1.hasNext();) {
					Cell cell = (Cell) iterator1.next();
					String headerName = getCellValue(cell) + "";
					// ignore roll number column name
					headers.add(headerName);

					System.out.println("subjectName = " + headerName);

				}

			} else {

				// System.out.println(" headers " + headers.size());
				// handle rest of the rows
				Map<String, String> tableValue = new HashMap<String, String>();

				short lastCellNum = nextRow.getLastCellNum();
				System.out.println(" lastCellNum " + lastCellNum);

				for (int i = 0; i < nextRow.getLastCellNum(); i++) {

					Cell cell = (Cell) nextRow.getCell(i);
					Object cellContent = getCellValue(cell);
					String headername = headers.get(i);

					System.out.println("values being mapped : " + i + " = " + cellContent + "  header : " + headername);

					if (cellContent == null || cellContent.equals("null")) {
						continue;
					}

					String subMarks = cellContent + "";
					System.out.println("values being mapped : " + headername + " = " + subMarks);
					tableValue.put(headername, subMarks);

				}

				stringMapList.add(tableValue);

			}
			counter++;
		}
		return stringMapList;
	}

	public static Object getCellValue(Cell cell) {
		if (cell == null) {
			return null;
		}

		switch (cell.getCellType()) {
		case Cell.CELL_TYPE_STRING:
			return cell.getStringCellValue();

		case Cell.CELL_TYPE_BOOLEAN:
			return cell.getBooleanCellValue();

		case Cell.CELL_TYPE_NUMERIC:
			return cell.getNumericCellValue();
		}

		return null;
	}
	
	@Override
    public List<String>  getHeadersOfExcel(Sheet firstSheet){
    	
        List<Map<String, String>> stringMapList = new ArrayList<Map<String, String>>();

        Iterator<Row> iterator = firstSheet.iterator();
        int counter = 0;

        List<String> headers = new ArrayList<String>();

        while (iterator.hasNext()) {

            Row nextRow = iterator.next();
            
                // handle header
                System.out.println(" Need to take subject names ");
//                Iterator<Cell> cellIterator = nextRow.cellIterator();
                int colCounter = 0;

                for (Iterator iterator1 = nextRow.cellIterator(); iterator1.hasNext();) {
                    Cell cell = (Cell) iterator1.next();
                    String headerName = getCellValue(cell) + "";
                    // ignore roll number column name 
                    headers.add(headerName);
                    System.out.println("subjectName = " + headerName);
                }
            break;
        }
        return headers;
    }

	@Override
	public JsonNode getTableAsJson(String excelSheetLocation) throws IOException {
		// TODO Auto-generated method stub
		System.out.println(" getTableAsJson ");
		List<Map<String, String>> tableAsListMap = getTableAsListMap(excelSheetLocation);
		String jsonString = new ObjectMapper().writeValueAsString(tableAsListMap);
		System.out.println("jsonString:" + jsonString);

		ObjectMapper mapper = new ObjectMapper();
		JsonNode actualObj = mapper.readTree(jsonString);

		return actualObj;
	}

}
