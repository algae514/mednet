package com.adamglobal.services;

import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.adamglobal.dao.DoctorDao;
import com.adamglobal.web.controller.DoctorController;
import com.adamglobal.web.model.Doctor;
import com.adamglobal.web.model.DoctorSearchCriterea;

@Service("doctorService")
@Transactional
public class DoctorServiceImpl implements DoctorService {

	@Autowired
	private DoctorDao dao;

	@Autowired
	MessageSource messageSource;
	Logger logger = Logger.getLogger(DoctorServiceImpl.class.getName());
	
	public Doctor findById(int id) {
		return dao.findById(id);
	}

	public void saveDoctor(Doctor Doctor) {
		dao.saveDoctor(Doctor);
	}

	/*
	 * Since the method is running with Transaction, No need to call hibernate
	 * update explicitly. Just fetch the entity from db and update it with
	 * proper values within transaction. It will be updated in db once
	 * transaction ends.
	 */
	public String updateDoctor(Doctor doctor) {
		// Doctor entity = dao.findById(Doctor.getId());
		Doctor entity = dao.findDoctorByUserId(doctor.getUserid());
		System.out.println(" fetched : doctor s from DB : :" + entity);

		if (entity != null) {
			entity.setAvlblTime(doctor.getAvlblTime());
			entity.setCt(doctor.getCt());
			entity.setImage(doctor.getImage());
			entity.setIssue(doctor.getIssue());
			entity.setLcn(doctor.getLcn());
			entity.setName(doctor.getName());
			entity.setPassword(doctor.getPassword());
			entity.setPrice(doctor.getPrice());
			entity.setQualfcn(doctor.getQualfcn());
			entity.setTrtmnt(doctor.getTrtmnt());
			logger.log(Level.INFO, "to be udpated : editProfile Doctor: "+entity);
			dao.updateDoctor(entity);
			return "SUCCESS";
		}else{
			return "invalid userid";	
		}
		

	}

	public void deleteDoctorByUserId(String userId) {
		Doctor findDoctorByUserId = dao.findDoctorByUserId(userId);
		int id = findDoctorByUserId.getId();
		deleteById(id);
	}

	public List<Doctor> findAllDoctors() {
		return dao.findAllDoctors();
	}

	public Doctor findDoctorByUserId(String userId) {
		return dao.findDoctorByUserId(userId);
	}

	public boolean isDoctorUserIdUnique(Integer id, String userId) {
		Doctor Doctor = findDoctorByUserId(userId);
		return (Doctor == null || ((id != null) && (Doctor.getId() == id)));
	}

	@Override
	public List<Doctor> searchDoctors(DoctorSearchCriterea criterea) {
		// TODO Auto-generated method stub
		return dao.searchDoctors(criterea);
	}

	@Override
	public List<String> getTreatments(){
		// TODO Auto-generated method stub
		return dao.getTreatments();
	}

	@Override
	public void deleteById(int ssn) {
		// TODO Auto-generated method stub
		dao.deleteById(ssn);

	}

	@Override
	public Doctor findDoctorByUserid(String userId) {
		// TODO Auto-generated method stub
		return dao.findDoctorByUserId(userId);
	}

	@Override
	public boolean isDoctorUseridUnique(String userId) {
		// TODO Auto-generated method stub
		Doctor findDoctorByUserId = dao.findDoctorByUserId(userId);
		return findDoctorByUserId == null ? true : false;
	}

	@Override
	public Doctor searchDoctorsAuth(Doctor doctor) {
		// TODO Auto-generated method stub
		return dao.searchDoctorsAuth(doctor);
	}

	@Override
	public void increaseLikes(String userId) {
		// TODO Auto-generated method stub
		logger.log(Level.INFO, "id : : "+userId);
		Doctor findById = dao.findDoctorByUserId(userId);
		System.out.println(" increaseLikes : findById : id : "+userId);
		logger.log(Level.INFO, " increaseLikes : findById : id : "+userId );
		findById.setRcmnds(findById.getRcmnds()+1);
		System.out.println(" increaseLikes : findById : "+findById);

		dao.updateDoctor(findById);
		
		logger.log(Level.INFO, " AFTER : increaseLikes : findById : "+findById );
		
	}

}