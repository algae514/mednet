package com.adamglobal.services;

import java.util.List;

import com.adamglobal.web.model.Appointment;
import com.adamglobal.web.model.AppointmentSearchCriterea;

public interface AppointmentService {


	Appointment findById(int id);

	void saveAppointment(Appointment Appointment);

	String updateAppointment(Appointment Appointment);

	void deleteById(int ssn);

	List<Appointment> findAllAppointments();

	Appointment findAppointmentByUserid(String userId);

	boolean isAppointmentUseridUnique(String userId);

	List<Appointment> searchAppointments(AppointmentSearchCriterea criterea);

	Appointment searchAppointmentsAuth(Appointment appointment);


}
