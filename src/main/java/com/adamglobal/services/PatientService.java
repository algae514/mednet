package com.adamglobal.services;

import java.util.List;

import com.adamglobal.web.model.Patient;
import com.adamglobal.web.model.PatientSearchCriterea;

public interface PatientService {


	Patient findById(int id);

	void savePatient(Patient Patient);

	String updatePatient(Patient Patient);

	void deleteById(int ssn);

	List<Patient> findAllPatients();

	Patient findPatientByUserid(String userId);

	boolean isPatientUseridUnique(String userId);

	List<Patient> searchPatients(PatientSearchCriterea criterea);

	Patient searchPatientsAuth(Patient patient);

}
