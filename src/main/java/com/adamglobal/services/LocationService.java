package com.adamglobal.services;

import java.util.List;

import com.adamglobal.web.model.Location;


public interface LocationService {

	Location findById(int id);

	void saveLocation(Location Location);

	String updateLocation(Location Location);

	void deleteById(int ssn);

	List<Location> findAllLocations();

	Location findLocationByUserid(String userId);

	boolean isLocationUseridUnique(String userId);


	List<Location> getSupportedCities();

	List<Location> getSupportedLocations(String city);


}
