package com.adamglobal.services;

import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.adamglobal.dao.LocationDao;
import com.adamglobal.web.model.Location;


@Service("locationService")
@Transactional
public class LocationServiceImpl implements LocationService {

	@Autowired
	private LocationDao dao;

	@Autowired
	MessageSource messageSource;
	Logger logger = Logger.getLogger(LocationServiceImpl.class.getName());
	
	public Location findById(int id) {
		return dao.findById(id);
	}

	public void saveLocation(Location Location) {
		dao.saveLocation(Location);
	}

	/*
	 * Since the method is running with Transaction, No need to call hibernate
	 * update explicitly. Just fetch the entity from db and update it with
	 * proper values within transaction. It will be updated in db once
	 * transaction ends.
	 */
	public String updateLocation(Location location) {
		// Location entity = dao.findById(Location.getId());
		Location entity = dao.findById(location.getId());
		System.out.println(" fetched : location s from DB : :" + entity);

		if (entity != null) {
			entity.setCt(location.getCt());
			entity.setLctn(location.getLcn());
			logger.log(Level.INFO, "to be udpated : editProfile Location: "+entity);
			dao.updateLocation(entity);
			return "SUCCESS";
		}else{
			return "invalid userid";	
		}
		

	}

	public void deleteLocationByUserId(String userId) {
		Location findLocationByUserId = dao.findLocationByUserId(userId);
		int id = findLocationByUserId.getId();
		deleteById(id);
	}

	public List<Location> findAllLocations() {
		return dao.findAllLocations();
	}

	public Location findLocationByUserId(String userId) {
		return dao.findLocationByUserId(userId);
	}

	public boolean isLocationUserIdUnique(Integer id, String userId) {
		Location Location = findLocationByUserId(userId);
		return (Location == null || ((id != null) && (Location.getId() == id)));
	}

	@Override
	public void deleteById(int ssn) {
		// TODO Auto-generated method stub
		dao.deleteById(ssn);

	}

	@Override
	public Location findLocationByUserid(String userId) {
		// TODO Auto-generated method stub
		return dao.findLocationByUserId(userId);
	}

	@Override
	public boolean isLocationUseridUnique(String userId) {
		// TODO Auto-generated method stub
		Location findLocationByUserId = dao.findLocationByUserId(userId);
		return findLocationByUserId == null ? true : false;
	}



	@Override
	public List<Location> getSupportedCities() {
		// TODO Auto-generated method stub
		return dao.getSupportedCities();
	}

	@Override
	public List<Location> getSupportedLocations(String city) {
		// TODO Auto-generated method stub
		return dao.getSupportedLocations(city);
	}


}