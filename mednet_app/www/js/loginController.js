appCore.controller('LoginCtrl', function($scope, $state, ajaxServices) {

    $scope.loginData = { userid: "", password: ""  , cpassword: "" };
    $scope.isReplyFormOpen = false;

    console.log('goToRegister() ' + $scope.loginData.login);


    $scope.register = function() {

        if (!validRegister()) {
            return;
        }


        ajaxServices.register($scope.loginData).then(function(response) {

            if (response.data.msg != null) {
                console.log(" failed to register ")
                alert(response.data.msg)
            } else {
                console.log(" completed data login success " + JSON.stringify(response.data.result));
                user_id = response.data.result[0].id;
                $state.go('app.search');
            }



        }, function(response) {
            alert(" failed to fetch login details, please contact admin.");

        });



    }

    function validRegister() {
        if (($scope.loginData.cpassword != $scope.loginData.password) || ($scope.loginData.password.trim().length == 0 ) ) {
            alert("Password and confirm password do not match.")
            return false;
        }

        if($scope.loginData.userid == null || $scope.loginData.userid.trim().length == 0){
         alert("User id cannot be empty.")
            return false;   
        }

        if($scope.loginData.cd == null || $scope.loginData.cd.trim().length == 0){
         alert("Code cannot be empty.")
            return false;   
        }

        return true;
    }
    $scope.doLogin = function() {
        // $scope.loginData = { "userid": "876678", "password": "kinngsay@123" }

        ajaxServices.loginpatient($scope.loginData).then(function(response) {

            if (response.data.result == null) {
                console.log(" failed to login ")
                alert("Invalid user name and password.")
            } else {
                console.log(" completed data login success " + JSON.stringify(response.data.result));
                user_id = response.data.result[0].id;
                $state.go('app.search');
            }



        }, function(response) {
            alert(" failed to fetch login details, please contact admin.");

        });

    }


    $scope.goToRegister = function() {
        $scope.loginData.login = false;
        console.log('goToRegister() ' + $scope.loginData.login);

    };


    $scope.goToLogin = function() {
        $scope.loginData.login = true;
        console.log('goToRegister() ' + $scope.loginData.login);

    };



})
