appCore.controller('DoctorPageCtrl', function($scope, $stateParams, ajaxServices) {
    var docId = { userid: $stateParams.DoctorlistId }
    $scope.dateInput = {};



    console.log(" doc id = " + docId)

    ajaxServices.viewProfile(docId).then(function successCallback(response) {
        console.log("Success with response : " + JSON.stringify(response.data));

        $scope.doctor = response.data.result[0];
        console.log("Success with response $scope.doctor : " + JSON.stringify($scope.doctor));
        if (response.data == "error") {
            alert(" Sorry some internal error has occured. ")
            return;
        }
    }, function errorCallback(response) {
        alert("Sorry some internl error occured. Please check your internet connection and contact admin.")
    });


    $scope.bookAppointment = function() {
        console.log(" need to book appointment " + $scope.dateInput.aptmntdt)

        if ($scope.dateInput.aptmntdt == null) {
            alert("Please select a date for appointment ");
            return;
        }

        var bookAppointmnet = { "dctrId": $scope.doctor.id, "ptntId": user_id, "fromTime": $scope.dateInput.aptmntdt, "toTime": $scope.dateInput.aptmntdt }

        bookAppointmnet.fromTime = ajaxServices.updateTimeFormat($scope.dateInput.aptmntdt);
        bookAppointmnet.toTime = ajaxServices.updateTimeFormat($scope.dateInput.aptmntdt);


        ajaxServices.bookAppointment(bookAppointmnet).then(function successCallback(response) {
            console.log("Success with response : " + JSON.stringify(response.data));
            alert(" Your appointment has been booked !");

        }, function errorCallback(response) {
            alert("Sorry some internl error occured. Please check your internet connection and contact admin.")
        });




    }

    $scope.increaseLikes = function() {

        $scope.doctor.likes++;
        ajaxServices.increaseLikes(docId);
        alert(" Thank you !!!");
    }


})