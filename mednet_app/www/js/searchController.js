appCore.controller('SearchCtrl', function($scope, $state, ajaxServices, utilServices) {

        $scope.searchData = { name: "", lcn: "", trtmnt: "", ct: "" };
        console.log(" in SearchCtrl");

        $scope.searchDataHelpers = {};
        $scope.searchDataHelpers.ct = [];
        $scope.searchDataHelpers.lcn = [];
        $scope.searchDataHelpers.trtmnt = [];
        retrievedValues = [];





        ajaxServices.getSupportedCities().then(function(response) {

            if (response.data.result == null) {
                console.log(" failed to login ")
                alert("Unable to fetch locations information. Please contact admin.")
            } else {
                console.log(" completed data login success " + JSON.stringify(response.data.result));
                retrievedValues = response.data.result;
                for (var i = 0; i < response.data.result.length; i++) {
                    if (!utilServices.exists($scope.searchDataHelpers.ct, response.data.result[i].ct)) {
                        $scope.searchDataHelpers.ct.push(response.data.result[i].ct)
                    }

                }
                console.log(" $scope.searchDataHelpers.ct " + JSON.stringify($scope.searchDataHelpers.ct))
                $state.go('app.search');
            }



        }, function(response) {
            alert(" failed to fetch login details, please contact admin.");

        });



        ajaxServices.getTreatments().then(function(response) {
            // console.log("  response "+JSON.stringify(response.data))

            if (response.data.length == 0) {
                console.log(" failed to login ")
                alert("Failed to fetch treatments, please contact admin.")
            } else {
                console.log(" completed data login success " + JSON.stringify(response.data));
                $scope.searchDataHelpers.trtmnt = response.data;
                console.log(" $scope.searchDataHelpers.ct " + JSON.stringify($scope.searchDataHelpers.trtmnt))
            }

        }, function(response) {
            alert(" failed to fetch login details, please contact admin.");
        });




        var earlier = "";
        $scope.cityChanged = function(index) {

            console.log(" selected city changed to " + $scope.searchData.ct)
            $scope.searchDataHelpers.lcn = utilServices.getSupportedLocations($scope.searchData.ct, retrievedValues);
            console.log(" $scope.searchDataHelpers.lcn " + JSON.stringify($scope.searchDataHelpers.lcn))
        }


        $scope.searchDoctors = function() {
            console.log("searchData.docName: " + $scope.searchData.docName + " searchData.locationName: " + $scope.searchData.lcn + " " + " - searchData.treatment: " + $scope.searchData.trtmnt + "  searchData.city: " + $scope.searchData.ct + " ");

            ajaxServices.search($scope.searchData).then(function successCallback(response) {
                console.log("Success with response : " + JSON.stringify(response.data));
                $scope.Doctorlists = response.data.result;

                if (response.data == "error") {
                    alert(" Sorry some internal error has occured. ")
                    return;
                }
            }, function errorCallback(response) {
                alert("Sorry some internl error occured. Please check your internet connection and contact admin.")
            });
            // $state.go('app.Doctorlists');
        }


        $scope.toDetails = function(index) {
            console.log(" index " + $scope.Doctorlists[index].userid)
                // $state.go()
            $state.go('app.doctorPage', { DoctorlistId: $scope.Doctorlists[index].userid });
        }



    });