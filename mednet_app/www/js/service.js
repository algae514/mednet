'use strict';
/**
 * Components that are must 
 * 
 * 
 */
angular.module('starter.ajaxServices' , []).service('ajaxServices', ['$http', function($http) {

    var registerUrl = centralServer + 'registerpatient'
    var loginpatientUrl = centralServer + 'loginpatient'
    var editProfileUrl = centralServer + 'editProfile'
    var searchUrl = centralServer + 'search'
    var viewProfileUrl = centralServer + 'viewProfile'
    var increaseLikesUrl = centralServer + 'increaseLikes'
    var bookAppointmentUrl = centralServer + 'bookappointment'

    var getTreatmentsUrl = centralServer + 'getTreatments'
    var getSupportedCitiesUrl = centralServer + 'getSupportedCities'
    var getSupportedLocationsUrl = centralServer + 'getSupportedLocations'

    /**
     @detail JSON must contain values {"name":"Dr. abc" , "userid" : "996678" , "password" : "kinngsay@123" , "image" : "http://imgur.com/included/incur" , "issue" : "Fever" , "price" : "150.25" , "ct" : "Vijaynagaram" , "lcn":"tadi"}
     * @param {type} app
     * @returns {promise}
     */
    this.getTreatments = function() {
        console.log(" in service getTreatments " );
        return $http.post(getTreatmentsUrl);
    }

/**
     @detail JSON must contain values {"name":"Dr. abc" , "userid" : "996678" , "password" : "kinngsay@123" , "image" : "http://imgur.com/included/incur" , "issue" : "Fever" , "price" : "150.25" , "ct" : "Vijaynagaram" , "lcn":"tadi"}
     * @param {type} app
     * @returns {promise}
     */
    this.getSupportedLocations = function(app) {
        console.log(" in service " + JSON.stringify(app));
        return $http.post(getSupportedLocationsUrl, app);
    }


    /**
     @detail JSON must contain values {"name":"Dr. abc" , "userid" : "996678" , "password" : "kinngsay@123" , "image" : "http://imgur.com/included/incur" , "issue" : "Fever" , "price" : "150.25" , "ct" : "Vijaynagaram" , "lcn":"tadi"}
     * @param {type} app
     * @returns {promise}
     */
    this.getSupportedCities = function(app) {
        console.log(" in service " + JSON.stringify(app));
        return $http.post(getSupportedCitiesUrl, app);
    }
    /**
     @detail JSON must contain values {"name":"Dr. abc" , "userid" : "996678" , "password" : "kinngsay@123" , "image" : "http://imgur.com/included/incur" , "issue" : "Fever" , "price" : "150.25" , "ct" : "Vijaynagaram" , "lcn":"tadi"}
     * @param {type} app
     * @returns {promise}
     */
    this.register = function(app) {
        console.log(" in service " + JSON.stringify(app));
        return $http.post(registerUrl, app);
    }

    /**
     @detail JSON must contain values {"userid" : "876678" , "password" : "kinngsay@123" }
     * @param {type} app
     * @returns {promise}
     */
    this.loginpatient = function(app) {
        console.log(" in service " + JSON.stringify(app));
        return $http.post(loginpatientUrl, app);
    }


    /**
     @detail JSON must contain values {"name":"Dr. abcd" , "userid" : "876678" , "password" : "kinngsay@1234" , "image" : "http://imgur.com/included/docpic" , "issue" : "hedache" , "price" : "150.25" , "ct" : "Vijaynagaram" , "lcn":"tadi"}
     * @param {type} app
     * @returns {promise}
     */
    this.editProfile = function(app) {
        console.log(" in service " + JSON.stringify(app));
        return $http.post(editProfileUrl, app);
    }

    /**
     @detail JSON must contain values {"name":"Dr. abcd" ,"trtmnt" : "hedache" , "price" : "150.25" , "ct" : "Vijaynagaram" , "lcn":"tadi"}
     * @param {type} app
     * @returns {promise}
     */
    this.search = function(app) {
        console.log(" in service " + JSON.stringify(app));
        return $http.post(searchUrl, app);
    }

    /**
     @detail JSON must contain values {"userid" : "876678" }
     * @param {type} app
     * @returns {promise}
     */
    this.viewProfile = function(app) {
        console.log(" in service " + JSON.stringify(app));
        return $http.post(viewProfileUrl, app);
    }

    /**
     @detail JSON must contain values {"userid" : "876678" }
     * @param {type} app
     * @returns {promise}
     */
    this.viewProfile = function(app) {
        console.log(" in service " + JSON.stringify(app));
        return $http.post(viewProfileUrl, app);
    }

    /**
     @detail JSON must contain values {"userid" : "876678" }
     * @param {type} app
     * @returns {promise}
     */
    this.increaseLikes = function(app) {
        console.log(" in service " + JSON.stringify(app));
        return $http.post(increaseLikesUrl, app);
    }

    /**
     @detail JSON must contain values {"userid" : "876678" }
     * @param {type} app
     * @returns {promise}
     */
    this.bookAppointment = function(app) {
        console.log(" in service bookAppointment " + JSON.stringify(app));
        return $http.post(bookAppointmentUrl, app);
    }

this.updateTimeFormat = function(d){

     var n = d.getFullYear()+"-"+d.getMonth()+"-"+d.getDate()+"T"+d.getHours()+":"+d.getMinutes()+":"+d.getSeconds();

    console.log(" dt "+n)

    return n;
}


}]);
